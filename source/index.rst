.. lai documentation master file, created by
   sphinx-quickstart on Tue Jun 22 18:31:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================
Kevin Lai
================

.. image:: https://i.imgur.com/CqJTPs6.gif

My name is Kevin and I am a dedicated Python and Javascript developer with a background in system engineering and integration. I have experience working on diverse in areas including Computational Chemistry, Civil Engineering, and Bioinformatics.


Personal Life
=============
In my free time, I love to play the violin and cello. I also like reading and swing dancing.

Thanks for visiting my portfolio. I hope this gives you a good sense of who I am as a professional and as an individual. Feel free to browse my projects and don't hesitate to reach out if you have any questions or collaboration ideas. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:
  
   projects/index
  


Contact
=======

* zlnh4@umsystem.edu
* 516-524-1521
* https://gitlab.com/bnetbutter
* https://github.com/bnetbutter


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
