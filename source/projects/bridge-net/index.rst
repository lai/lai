==========
Bridge Net
==========

The goal of this web application is to provide a platform for bridge inspectors
to upload and share bridge inspection data. I worked on this project during my time 
as a software engineer for a Civil Engineering Lab. The Lab's focus is on researching
new ways of using aerial drones to assist in bridge inspection.

.. image:: https://i.imgur.com/UtyKDiG.png
    

Workflow Engine Integration
---------------------------

Bridge Net integrates with a workflow engine built using React Flow to allow inspectors
to define the algorithms they want to run on their inspection data. This allows
for flexibility in the types of algorithms that can be run on the data as well as the data type.

.. image:: https://i.imgur.com/gglWpUZ.gif

In this example, the inspector has defined a workflow that splits a video into
frames, then runs a defisheye algorithms on the frames to remove the fisheye effect.
Drone captured footage often has a fisheye effect that can make it difficult for
algorithms to interpret the data.


The inspector can define the input by clicking on the node and select the resource they want to use. The form library I'm using is SurveyJS. 
It has a useful feature that allows form options to be dynamically generated based on a REST API call and it will throw an error if a resource is not found.

.. image:: https://i.imgur.com/z63jrmp.gif

Each node has a different type of form (or possibly none at all)
depending on the type of workitem it is. For example, the "Split Video" node has a form that allows the user to select the video they want to split. 
While Mask RCNN has a form that allows the user to select the bridge they want to run the algorithm on.


The user can also inspect the progress of the workflow as it runs.

.. image:: https://i.imgur.com/3BNZKNU.gif

For demonstration purposes, we have defined a simple workflow that uses a mock worker which does nothing but waits for 5 seconds.
The user can see the progress of the workflow. Each node represents an individual step in the workflow. The nodes are color coded to
reflect the status of the workitem.

- Blue means running
- Red means failed
- Yellow means queued
- Green means completed


Mask RCNN For Image Segmentation
--------------------------------

Bridge Net integrates with Mask RCNN machine learning model to detect and segment
bridge elements from images. This allows inspectors to quickly search through
inspection images and find the elements they are looking for

.. image:: https://i.imgur.com/HLSKDPg.gif

This gif shows Mask RCNN segmenting a bridge video frame by frame. But how does it
integrate into Bridge Net?

.. image:: https://i.imgur.com/a1YL1zx.gif

The user can select the bridge they want to run the algorithm on and the algorithm will generate polygons.
The polygons are injected into a annotator application. The user can then inspect the polygons and make any necessary adjustments.
For example, they can chose to delete any outlandlishly poor predictions or add any missing predictions.

Inspection Framework
--------------------

Bridge Net provides a framework for inspectors to annotate the conditions of a bridge element. After the inspector uses Mask RCNN to categorize each element
in a bridge, they can use the inspection application to search for the element they are interested in and annotate its condition.


.. image:: https://i.imgur.com/cood7io.gif


Tech Stack
----------

Here we get to the more technical part of the portfolio. The tech stack for Bridge Net is as follows:

- Frontend: React, React Router, SurveyJS, React Flow, Material UI
- Backend: PostgreSQL, Django Rest Framework, Celery, Redis
- Machine Learning: Mask RCNN, OpenCV, PyTorch
- Deployment: Docker Compose


Since we effectively covered the front end, let's briefly about the backend.

Database Schema
---------------

The database schema is shown below. I'm using DBeaver to capture the ER Diagram. DBeaver here is connected to the same PostgreSQL database that Bridge Net uses.

.. image:: https://i.imgur.com/iLgLUY6.png

The database schema looks a lot more complicated than it actually is because it contains connections from a few iterations of the data model  Every entity is linked to a Bridge in some way. Images are linked to a bridge,
elements and conditions are linked to a bridge. For example, the first iteration looked like this:

.. image:: https://i.imgur.com/nxgzUxc.png

Where each bridge has multiple elements, and each element has one image. This was not flexible enough for the use case. For example, a bridge can have multiple images. And an element can have multiple images and an image can have multiple elements.
So I had to change the data model to look like this:


.. image:: https://i.imgur.com/WiJFCY7.png

A junction table joins these to map a many to many relationships between the entities. The database looks a bit messy because it has foreign keys that reflects a combination of both data models.



Workflow Engine Architecture
----------------------------

The workflow engine starts by taking the graph definition and converting it into a directed acyclic graph. The graph is then traversed in a topological order via a depth first search algorithm.
Each visited node is queued in a Celery task. The task is executed inside a docker container. Each workitem has its own docker container. So multiple workers can be executed in a single docker container.
I put them in a separate docker container to avoid any potential conflicts between the workers especially when it comes to different package dependencies.
Machine learning models can be extremely picky in the version of the packages they use.

.. image:: https://i.imgur.com/oE3ocq7.png

All work items are queued in Celery, backed by Redis database. The MongoDB database is no longer in use. 
Originally I thought about using Mongo to store the workflow graph.  But it added unnecessary complexity to the project. 
The graph is now stored in the PostgreSQL database.


Docker Integration
------------------

The entire project is dockerized. The frontend, backend, and the workflow engine are all dockerized. 

- Front End Development Container
- Django Server Container
- PostgreSQL Database Container
- Redis Container

Docker containers are all mounted to a local directory so no rebuild is necessary during development The containers are all built and orchestrated by Docker Compose. Originally I
was using Kubernetes but it was overkill for the project especially since I had no need for load balancing. But Kubernetes did introduce me to the concept of container
orchestration. The entire web application can be started with one call to docker compose

.. image:: https://i.imgur.com/KON0Qxb.gif

