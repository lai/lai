Projects
===========



.. toctree::
    :maxdepth: 2
    :caption: Non-University Projects:
    
    hwk-system/hwk-system
    smart-mirror/index
    auto-host/index
    compile-graph/index
    this-portfolio/index

.. toctree::
    :maxdepth: 2
    :caption: University Projects:
    
    bridge-inspection-analytics/index
    bridge-net/index
    rolla-quest-3d/rolla-quest-3d
    py2cfg/py2cfg
    grade.sh/grade.sh

    
    